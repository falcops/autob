## [0.1.8](https://gitlab.com/falcops/autob/compare/v0.1.7...v0.1.8) (2022-04-03)


### Bug Fixes

* don't run for tags ([4902b4a](https://gitlab.com/falcops/autob/commit/4902b4a52a467afc7b7ff6ee0656df9aeb578260))

## [0.1.7](https://gitlab.com/falcops/autob/compare/v0.1.6...v0.1.7) (2022-04-03)


### Bug Fixes

* try new glob ([48b7b91](https://gitlab.com/falcops/autob/commit/48b7b91c35394bcd0ab37fd3b8455e67b95c30e9))

## [0.1.6](https://gitlab.com/falcops/autob/compare/v0.1.5...v0.1.6) (2022-04-03)


### Bug Fixes

* try to catch all changes from tbump ([709cfd7](https://gitlab.com/falcops/autob/commit/709cfd7f7f0753b75e533978076213c36b0d317b))

## [0.1.5](https://gitlab.com/falcops/autob/compare/v0.1.4...v0.1.5) (2022-04-03)


### Bug Fixes

* use a prepareCmd instead ([35652a8](https://gitlab.com/falcops/autob/commit/35652a84b4a4b5ad3dc4a629559dc4d77569944c))

## [0.1.4](https://gitlab.com/falcops/autob/compare/v0.1.3...v0.1.4) (2022-04-03)


### Bug Fixes

* try now ([c234be8](https://gitlab.com/falcops/autob/commit/c234be8c5ae781c56e73203848cdaa74a8d9bb8f))

## [0.1.3](https://gitlab.com/falcops/autob/compare/v0.1.2...v0.1.3) (2022-04-03)


### Bug Fixes

* what? ([7ece6d7](https://gitlab.com/falcops/autob/commit/7ece6d70807e71074c0937ea995574685e88e1bd))

## [0.1.2](https://gitlab.com/falcops/autob/compare/v0.1.1...v0.1.2) (2022-04-03)


### Bug Fixes

* will it work ([cb44767](https://gitlab.com/falcops/autob/commit/cb447678751c46e5c14dc8586fed65776d6cdcde))

## [0.1.1](https://gitlab.com/falcops/autob/compare/v0.1.0...v0.1.1) (2022-04-03)


### Bug Fixes

* and now it works ([7ccbe4b](https://gitlab.com/falcops/autob/commit/7ccbe4b6687e509b5a3a06d5093cd5adf5a973eb))
* test ([48dc68a](https://gitlab.com/falcops/autob/commit/48dc68aaa96858bdade5c51acc8eba47f762af8c))
